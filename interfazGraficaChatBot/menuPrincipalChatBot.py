# import everything from tkinter module 
from tkinter import *    
import os
import tkinter as tk
try:
    import ttk as ttk
    import ScrolledText
except ImportError:
    import tkinter.ttk as ttk
    import tkinter.scrolledtext as ScrolledText
from tkinter.messagebox import showinfo
import webbrowser
from PIL import Image, ImageTk
from tkinter import simpledialog
import sys
sys.path.append('../reconocimientoFacial/')




class menuPrincipalBot(tk.Tk):

	EJECUTAR_STREAMING_RECONOCIMIENTO='cd ../reconocimientoFacial/ && python recognize_video.py --detector face_detection_model \
	--embedding-model openface_nn4.small2.v1.t7 \
	--recognizer output/recognizer.pickle \
	--le output/le.pickle'

	REGISTRAR_ROSTRO='cd ../registrarRostroDataset/ && python build_face_dataset.py --cascade haarcascade_frontalface_default.xml \
	--output dataset/'
	
	def __init__(window, *args, **kwargs):
		#Se inicializan las variables.
		tk.Tk.__init__(window, *args, **kwargs)	

		#Se pone el título
		window.title('Menú principal')
		window.initialize()



	"""
	    Método encargado de inicializar todo el gui.
	"""    
	def initialize(window):
	    window.centrarPantallaPrincipal()
	    window.grid()
	    showinfo("Instrucciones de uso", "Hola, este bot cuenta con información que podría ser útil. \n Para poder usarlo debes estar registrado. Si ya estás registrado solamente debes dar clic en el botón Login. \n Si aún no estás registrado debes dar clic en el botón registrarse y seguir las instrucciones.")

	    #Se carga la imagen de cada botón	
	    buttonImageLogin = Image.open('images/login.png')
	    window.buttonPhotoLogin = ImageTk.PhotoImage(buttonImageLogin) 
	    # Botón para realizar el login con reconocimiento
	    window.btnStreamingReconocimiento = ttk.Button(window, image=window.buttonPhotoLogin, text='Login con reconocimiento', command=window.ejecutarReconocimiento, compound="top")
	    window.btnStreamingReconocimiento.place(relx=0.5, rely=0.2, anchor=CENTER)
	    buttonImage = Image.open('images/bot_small.png')

	    buttonImageRegister = Image.open('images/newUser.png')
	    window.buttonPhotoRegister = ImageTk.PhotoImage(buttonImageRegister) 

		# Botón para realizar el registro del reconocimiento
	    window.btnRegistrarRostro = ttk.Button(window, image=window.buttonPhotoRegister, text='Registrar rostro', command=window.registrarRostro, compound="top")
	    window.btnRegistrarRostro.place(relx=0.5, rely=0.4, anchor=CENTER)
	    
	    #Se carga la imagen de cada botón	
	    buttonImageBot = Image.open('images/bot_small.png')
	    window.buttonPhotoBot = ImageTk.PhotoImage(buttonImageBot) 
	
	    window.btnAbrirBot = ttk.Button(window, image=window.buttonPhotoBot, text='Abrir bot', padding='10 10 10 10', command=window.abrirBotURL, compound="top")
	    window.btnAbrirBot.place(relx=0.5, rely=0.6, anchor=CENTER)
	    window.btnAbrirBot.state(["disabled"])   # Disable the button.

	    window.btnAbrirBot.grid_forget()

	    buttonImageExit = Image.open('images/exit.png')
	    window.buttonPhotoExit = ImageTk.PhotoImage(buttonImageExit) 

	    # Botón para salir
	    window.btnSalir = ttk.Button(window, image=window.buttonPhotoExit, text='Salir', command=window.destroy, compound="top")
	    window.btnSalir.place(relx=0.5, rely=0.8, anchor=CENTER)
	    





	def abrirBotURL(window):
		webbrowser.open('http://localhost:5002/guest/conversations/production/cd3f8564a3ea4e608449649e933a16c7')

	def registrarRostro(window):

		nombre_usuario = simpledialog.askstring(title="Registrar rostro", prompt="Cuál es tu nombre(Sin espacios ni caracteres):")
		os.mkdir('../registrarRostroDataset/dataset/'+nombre_usuario)
		os.system('cd ../registrarRostroDataset/ && python build_face_dataset.py --cascade haarcascade_frontalface_default.xml \
	--output dataset/'+nombre_usuario)

	def ejecutarReconocimiento(window):
			os.system('cd ../reconocimientoFacial/ && python recognize_video.py --detector face_detection_model \
		--embedding-model openface_nn4.small2.v1.t7 \
		--recognizer output/recognizer.pickle \
		--le output/le.pickle')  

	def centrarPantallaPrincipal(window):
		# Se obtiene el ancho y alto de la pantalla.
		windowWidth = window.winfo_reqwidth()
		windowHeight = window.winfo_reqheight()
		print("Width",windowWidth,"Height",windowHeight)
		 
		# Se obtienen los centros de la pantalla.
		positionRight = int(window.winfo_screenwidth()/2 - windowWidth/2)
		positionDown = int(window.winfo_screenheight()/2 - windowHeight/2)
		 
		# Se posiciona la pantalla principal en la mitad.
		window.geometry("900x600")
		window.geometry("+{}+{}".format(positionRight, positionDown))

def hola(evento):
	posX =evento.x
	posY =evento.y
	print(posX)
	print(posY)

interfaz = menuPrincipalBot()
interfaz.bind("<Button-1>", hola)
interfaz.mainloop()	 